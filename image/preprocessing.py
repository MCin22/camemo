import random
from scipy import ndimage

import cv2
import numpy as np
from skimage import exposure
from skimage.exposure import rescale_intensity
from sklearn.decomposition import PCA
from scipy.cluster.vq import whiten

from ui.right_dock import RightDockChildWidget


class PreprocessingModule:
    def __init__(self):
        self.initialize_module()

    def initialize_module(self):
        # self.mask = cv2.imread('resources/masks/mask.png', 0)
        self.mask = cv2.imread('resources/masks/mask_nose.png', 0)
        pass

    def preprocess(self, image):
        # wyciecie fragmentu twarzy
        w, h = image.shape[1], image.shape[0]
        image = image[int(1.0 / 8 * h):h, int(1.0 / 4 * w): -1 * int(1.0 / 4 * w)]

        # przywrocenie rozmiaru obrazu
        from core import Core
        image = cv2.resize(image, (
        Core.modules['opencv_webcam'].cropped_size.width(), Core.modules['opencv_webcam'].cropped_size.height()))

        # zmiana typu na float
        image = image.astype('float32')
        # image /= 255

        # globalna normalizacja kontrastu
        # image = self.global_contrast_normalization(image)

        # zca whitening
        # image = self.flatten_matrix(image)
        # image = self.zca_whitening(image)
        # image = self.deflatten_matrix(image)
        ## image[image > 1] = 1. - image[image > 1]
        ## image[image < 0] = 1. + image[image < 0]

        # przywrocenie typu na float
        # image *= 255

        # korekcja gamma
        image = cv2.pow(image, 0.2)

        # image = self.diff_gaussian(image, 0.1, 1.4)

        # Odjecie rozmycia Gaussa
        image2 = self.gaussian_blur(image, (5,5), 5.0)
        image = cv2.subtract(image, image2)

        # SSR - Single Scale Retinex  http://www.algorytm.org/przetwarzanie-obrazow/retinex.html
        # image = self.ssretinex(image)

        # MSRCR - Multi Scale Retinex with Color Restoration - dla skali odcieni szarosci (przywrocenie dynamiki obrazu)
        # image = self.msretinex(image)

        # standaryzacja obrazu
        # image = self.standarize(image)

        # globalna normalizacja kontrastu
        # image /= 255
        # image = self.global_contrast_normalization(image)
        # image *= 255

        # normalizacja intensywnosci obrazu
        image = self.normalize_intensity(image, 0.0, 255.0)

        image = image.astype('uint8')

        # wyrownanie histogramu (poniewaz zdjecia maja po obrobce rozna jasnosc) i ponowna normalizacja intensywnosci
        image = cv2.equalizeHist(image)
        image = self.normalize_intensity(image, 60.0, 150.0)
        image = image.astype('uint8')

        return image

    def standarize(self, image):
        # samplewise_center - set image mean to 0
        image -= np.mean(image)
        # samplewise_std_normalization - divide image by its std
        image /= np.std(image)
        return image

    def msretinex(self, image):
        alfa = 60.0
        beta = 1.1
        weight = 0.33
        image2 = self.gaussian_blur(image, (5,5), 0.0)
        imageR = 0.33 * np.log(image/image2)
        image = beta * np.log(alfa * image) * imageR
        return image

    def ssretinex(self, image):
        image2 = self.gaussian_blur(image, (5,5), 5.0)
        image = np.log(image/image2)
        return image

    def normalize_intensity(self, image, min=0.0, max=255.0):
        new_min = min
        new_max = max
        y = image.astype(np.float, copy=False)
        y -= image.min()
        y *= (new_max - new_min) / (image.max() - image.min())
        y += new_min
        image[:] = y
        return image

    def gaussian_blur(self, img, kernel_size=(5,5), sigma=5.0):
        blur = cv2.GaussianBlur(img, kernel_size, sigma)
        return blur

    def diff_gaussian(self, img, sigma0=0.1, sigma1=1.1):
        blur1 = cv2.GaussianBlur(img, (0,0), sigma0)
        blur2 = cv2.GaussianBlur(img, (0,0), sigma1)
        result = cv2.subtract(blur2, blur1)
        return result

    def global_contrast_normalization(self, X):
        ddof = 1
        sqrt_bias = 0.
        min_divisor = 1e-8
        normalizers = np.sqrt(sqrt_bias + (X ** 2).sum(axis=1))
        normalizers[normalizers < min_divisor] = 1
        X /= normalizers[:, np.newaxis]
        return X

    def zca_whitening_alternative(self, image):
        image = image.astype('float32')
        image /= 255
        image = self.flatten_matrix(image)

        pca = PCA(whiten=True)
        image = pca.fit_transform(image)
        pca.whiten = False
        image = pca.inverse_transform(image)

        image = self.deflatten_matrix(image)
        image *= 255
        image = image.astype('uint8')
        return image

    def zca_whitening(self, inputs):
        sigma = np.dot(inputs, inputs.T) / inputs.shape[1]  # Correlation matrix
        U, S, V = np.linalg.svd(sigma)  # Singular Value Decomposition
        epsilon = 10e-6  # 10e-6        # Whitening constant, it prevents division by zero
        ZCAMatrix = np.dot(np.dot(U, np.diag(1. / np.sqrt(np.diag(S) + epsilon))), U.T)  # ZCA Whitening matrix
        return np.dot(ZCAMatrix, inputs)

    def flatten_matrix(self, matrix):
        # vector = matrix.flatten(1)
        # vector = vector.reshape(1, len(vector))
        vector = matrix.reshape(1, matrix.shape[0] * matrix.shape[1])
        return vector

    def deflatten_matrix(self, vector):
        from core import Core
        matrix = vector.reshape(Core.modules['opencv_webcam'].cropped_size.width(),
                                Core.modules['opencv_webcam'].cropped_size.height())
        return matrix

    def rotate(self, image, max_angle, fill_mode='nearest', cval=0.):
        angle = random.uniform(-max_angle, max_angle)
        rotated_image = ndimage.interpolation.rotate(image, angle,
                                                     axes=(0, 1),
                                                     reshape=False,
                                                     mode=fill_mode,
                                                     cval=cval)
        return rotated_image

    def preprocess_multiple(self, images):
        preprocessed_images = []
        for image in images:
            image = self.preprocess(image)
            preprocessed_images.append(image)

        return preprocessed_images

    def create_rotated_images(self, images, amount=2, angle=10):
        if amount == 0:
            pass
        rotated_images = []
        for image in images:
            rotated_images.append(image)
            for i in range(0, amount):
                rotated_image = self.rotate(image, angle)
                rotated_images.append(rotated_image)
        return rotated_images

    def create_flipped_images(self, images, horizontal_flip=True, vertical_flip=False):
        flipped_images = []
        for image in images:
            flipped_images.append(image)
            if horizontal_flip:
                h_flip = np.empty_like (image)
                h_flip[:] = image
                h_flip = self.horizontal_flip(h_flip)
                flipped_images.append(h_flip)
            if vertical_flip:
                v_flip = np.empty_like (image)
                v_flip[:] = image
                v_flip = self.vertical_flip(v_flip)
                flipped_images.append(v_flip)
        return flipped_images

    def horizontal_flip(self, x):
        x = np.fliplr(x)
        return x

    def vertical_flip(self, x):
        x = np.flipud(x)
        return x

    def shift_images(self, images, value=0.1):
        shifted_images = []
        for image in images:
            shifted_image = self.random_shift(image, value, value)
            shifted_images.append(shifted_image)
        return shifted_images

    def mask_images(self, images):
        masked_images = []
        for image in images:
            masked_image = self.mask_image(image)
            masked_images.append(masked_image)
        return masked_images

    def mask_image(self, image):
        image = cv2.bitwise_and(image,image,mask = self.mask)
        return image

    def random_shift(self, x, wrg, hrg, fill_mode="nearest", cval=0.):
        crop_left_pixels = 0
        crop_top_pixels = 0

        if wrg:
            crop = random.uniform(-wrg, wrg)
            split = random.uniform(0, 1)
            crop_left_pixels = int(split*crop*x.shape[0])
        if hrg:
            crop = random.uniform(-hrg, hrg)
            split = random.uniform(0, 1)
            crop_top_pixels = int(split*crop*x.shape[1])
        x = ndimage.interpolation.shift(x, (crop_left_pixels, crop_top_pixels),
                                        order=0,
                                        mode=fill_mode,
                                        cval=cval)
        return x