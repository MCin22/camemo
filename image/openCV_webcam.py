import cv2
import math

import sys
from PySide.QtCore import QSize, QTimer
from PySide.QtGui import QImage, QPixmap
import numpy as np


class OpenCVWebCamModule:
    def __init__(self):
        self.camera_number = 2
        self.flipY = False
        self.flipX = True
        self.colorBGRtoRGB = False

        self.initialize_module()

    def initialize_module(self):
        self.webcam_record_true = True
        self.video_size = QSize(640, 480)
        self.cropped_size = QSize(32, 32)
        self.capture = cv2.VideoCapture(self.camera_number)
        self.faceCascade = cv2.CascadeClassifier("resources/cascades/haarcascade_frontalface_default.xml")
        if not self.capture:
            print "Blad VideoCapture: nie udalo sie otworzyc urzadzenia " + str(self.camera_number)
            # sys.exit(1)
        else:
            self.setup_camera()

    def setup_camera(self):
        self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, self.video_size.width())
        self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self.video_size.height())

        self.timer = QTimer()
        self.timer.timeout.connect(self.display_video_stream)
        self.timer.start(84)
        self.timer.setInterval(84)

    def release_camera(self):
        self.timer.stop()
        self.capture.release()

    def display_video_stream(self):
        if self.webcam_record_true:
            ret, frame = self.capture.read()

            if self.colorBGRtoRGB:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            if self.flipY:
                frame = cv2.flip(frame, 0)
            if self.flipX:
                frame = cv2.flip(frame, 1)

            x, y, w, h = self.get_face_coordinates(frame)

            cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 3)

            image = QImage(frame, frame.shape[1], frame.shape[0],
                           frame.strides[0], QImage.Format_RGB888)

        from core import Core
        if self.webcam_record_true:
            Core.ui['center_dock'].webcam.setPixmap(QPixmap.fromImage(image))

    def get_face_coordinates(self, frame, gray_bool=True):
        if gray_bool is True:
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        else:
            gray = frame

        faces = self.faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(120, 120),
            flags=cv2.CASCADE_SCALE_IMAGE
        )

        for (x, y, w, h) in faces:
            return x, y, w, h

        return 0, 0, 0, 0

    def take_photo(self):
        ret, img = self.capture.read()
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        if self.colorBGRtoRGB:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        if self.flipY:
            img = cv2.flip(img, 0)
        if self.flipX:
            img = cv2.flip(img, 1)

        x, y, w, h = self.get_face_coordinates(img)
        if w > 0 and h > 0:
            face_img = img[y:y+h, x:x+w]
            face_img = self.denoise(face_img, False)
            face_img = cv2.resize(face_img, (self.cropped_size.width(), self.cropped_size.height()))
            return face_img
            # cv2.imwrite("test.png", resized_face_img)
        else:
            return None

    def take_photos(self, amount):
        list_photos = []
        for i in range(0, amount):
            photo = None
            while photo is None:
                photo = self.take_photo()
            list_photos.append(photo)
            # cv2.imwrite("test"+str(i)+".png", photo)
        return list_photos

    def denoise(self, frame, color=False):
        if color is False:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame = cv2.fastNlMeansDenoising(frame, None, 3, 7, 21)
        elif color is None:
            frame = cv2.fastNlMeansDenoising(frame, None, 3, 7, 21)
        else:
            frame = cv2.fastNlMeansDenoisingColored(frame, None, 3, 3, 7, 21)
        return frame

    def read_image(self, path):
        im = cv2.imread(path, 1)
        return im
