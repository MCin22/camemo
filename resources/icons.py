from PySide.QtGui import QIcon


class Icons:
    def __init__(self):
        self.iconNeutral = QIcon('resources/icons/neutral.png')
        self.iconJoy = QIcon('resources/icons/joy.png')
        self.iconSadness = QIcon('resources/icons/sadness.png')
        self.iconAnger = QIcon('resources/icons/anger.png')
        self.iconDisgust = QIcon('resources/icons/disgust.png')
        self.iconShocked = QIcon('resources/icons/shocked.png')

    def initialize_module(self):
        pass