from PySide.QtCore import Qt
from PySide.QtGui import QDockWidget, QScrollArea


class BaseDockWidget(QDockWidget):
    def __init__(self, *args, **kwargs):
        super(BaseDockWidget, self).__init__(*args, **kwargs)
        self.setFeatures(QDockWidget.DockWidgetMovable)
