# -*- coding: utf-8 -*-
import ntpath
from PySide import QtCore, QtGui
from PySide.QtGui import QWidget, QVBoxLayout, QFormLayout, QStandardItemModel, QComboBox, QStandardItem, QLabel, \
    QLineEdit
from core import Core
from ui.base_dock import BaseDockWidget
from users.user import User


class UserDockWidget(BaseDockWidget):
    def __init__(self, *args, **kwargs):
        super(UserDockWidget, self).__init__(*args, **kwargs)
        self.setWindowTitle(self.trUtf8("Użytkownicy"))

        self.dockChildWidget = UserDockChildWidget()
        self.setWidget(self.dockChildWidget)

    def get_child_widget(self):
        return self.dockChildWidget


class UserDockChildWidget(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.verticalLayout = QVBoxLayout(self)
        self.formLayout = QFormLayout()
        self.verticalLayout.addLayout(self.formLayout)

        self.labelUserName = QLabel(self.trUtf8("Użytkownik:"))
        self.comboBox = QComboBox()
        self.formLayout.addRow(self.labelUserName, self.comboBox)
        self.comboBoxModel = QStandardItemModel(self.comboBox)
        self.comboBox.setModel(self.comboBoxModel)
        self.comboBox.currentIndexChanged.connect(self.on_comboBox_index_changed)

        self.labelUserName = QLabel(self.trUtf8("Nazwa:"))
        self.lineEditUserName = QLineEdit()
        self.formLayout.addRow(self.labelUserName, self.lineEditUserName)
        self.lineEditUserName.textChanged.connect(self.on_user_name_edit)

        self.buttonAddNewPerson = QtGui.QPushButton(self.trUtf8("Dodaj"))
        self.buttonAddNewPerson.clicked.connect(self.add_new_person)
        self.buttonRemovePerson = QtGui.QPushButton(self.trUtf8("Usuń"))
        self.buttonRemovePerson.clicked.connect(self.remove_person)
        self.formLayout.addRow(self.buttonRemovePerson, self.buttonAddNewPerson)

        self.buttonImportBase = QtGui.QPushButton(self.trUtf8("Wczytaj bazę z pliku"))
        self.formLayout.addRow(self.buttonImportBase)
        self.buttonImportBase.clicked.connect(self.import_base)

        self.buttonExportBase = QtGui.QPushButton(self.trUtf8("Zapisz bazę do pliku"))
        self.formLayout.addRow(self.buttonExportBase)
        self.buttonExportBase.clicked.connect(self.export_base)

        self.create_users_from_Core()
        if len(Core.users) == 1:
            self.buttonRemovePerson.setEnabled(False)

    def import_base(self):
        path, filter = QtGui.QFileDialog.getOpenFileName(None, "Wczytaj plik bazy", '.')
        if path is not None and path != "":
            Core.import_user_base_from_file(path)
        self.buttonRemovePerson.setEnabled(True)

    def export_base(self):
        path, filter = QtGui.QFileDialog.getSaveFileName(None, "Zapisz plik bazy", '.')
        if path is not None and path != "":
            Core.export_user_base_from_file(path)

    def create_users_from_Core(self):
        self.comboBoxModel.clear()
        for user in Core.users:
            item = QStandardItem(user.name)
            item.setData(user.name, QtCore.Qt.DisplayRole)
            item.setData(user, QtCore.Qt.UserRole)
            self.comboBoxModel.appendRow(item)

    def on_comboBox_index_changed(self, index):
        Core.current_user = Core.users[index]
        Core.current_user_index = index
        self.lineEditUserName.setText(Core.current_user.name)
        Core.ui['right_dock'].get_child_widget().create_listView_from_current_user_samples()
        # Core.ui['right_dock'].get_child_widget().set_record_load_buttons(False)
        # Core.ui['left_dock'].get_child_widget().disable_buttons()

    def on_user_name_edit(self, text):
        if Core.current_user_index > -1:
            Core.current_user.name = text
            item = self.comboBoxModel.item(Core.current_user_index)
            item.setData(text, QtCore.Qt.DisplayRole)

    def add_new_person(self):
        user = User()
        Core.users.append(user)
        item = QStandardItem(user.name)
        item.setData(user.name, QtCore.Qt.DisplayRole)
        item.setData(user, QtCore.Qt.UserRole)
        self.comboBoxModel.appendRow(item)
        self.buttonRemovePerson.setEnabled(True)

    def remove_person(self):
        if len(Core.users) == 2:
            self.buttonRemovePerson.setEnabled(False)
        Core.users.remove(Core.users[Core.current_user_index])
        self.comboBoxModel.removeRow(Core.current_user_index)
        Core.current_user_index = self.comboBox.currentIndex()
        Core.current_user = Core.users[Core.current_user_index]
        Core.ui['right_dock'].get_child_widget().create_listView_from_current_user_samples()
