# -*- coding: utf-8 -*-
import ntpath
import os

import cv2
from PySide import QtGui, QtCore
from PySide.QtCore import QSize
from PySide.QtGui import QWidget, QVBoxLayout, QFormLayout, QLabel, QLineEdit, QListView, QStandardItemModel, \
    QStandardItem, QAbstractItemView, QIcon, QImage, QPixmap
import thread
from image.image import Image
from resources.icons import Icons
from ui.base_dock import BaseDockWidget


class RightDockWidget(BaseDockWidget):
    def __init__(self, *args, **kwargs):
        super(RightDockWidget, self).__init__(*args, **kwargs)
        self.setWindowTitle("Obrazy")

        self.dockChildWidget = RightDockChildWidget()
        self.setWidget(self.dockChildWidget)

    def get_child_widget(self):
        return self.dockChildWidget


class RightDockChildWidget(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.verticalLayout = QVBoxLayout(self)
        self.formLayout = QFormLayout()
        self.verticalLayout.addLayout(self.formLayout)

        self.labelEmotion = QtGui.QLabel(self.trUtf8("Emocja:"), self)
        self.comboBoxEmotion = QtGui.QComboBox(self)

        from core import Core
        self.comboBoxEmotion.addItem(Core.modules['icons'].iconNeutral, self.trUtf8("Neutralność"), 'neutral')
        self.comboBoxEmotion.addItem(Core.modules['icons'].iconJoy, self.trUtf8("Radość"), 'joy')
        self.comboBoxEmotion.addItem(Core.modules['icons'].iconSadness, self.trUtf8("Smutek"), 'sadness')
        self.comboBoxEmotion.addItem(Core.modules['icons'].iconAnger, self.trUtf8("Wściekłość"), 'anger')
        self.comboBoxEmotion.addItem(Core.modules['icons'].iconDisgust, self.trUtf8("Obrzydzenie"), 'disgust')
        self.comboBoxEmotion.addItem(Core.modules['icons'].iconShocked, self.trUtf8("Zaskoczenie"), 'shocked')
        self.comboBoxEmotion.setIconSize(QSize(40, 40))
        self.comboBoxEmotion.currentIndexChanged[int].connect(self.comboBox_selected)
        self.formLayout.addRow(self.labelEmotion, self.comboBoxEmotion)

        self.listView = QListView()
        self.formLayout.addRow(self.listView)
        self.listViewModel = QStandardItemModel(self.listView)
        self.listView.setIconSize(QSize(40, 40))
        self.listView.setModel(self.listViewModel)
        self.listViewSelectionModel = self.listView.selectionModel()
        self.listView.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.listView.clicked.connect(self.on_listView_clicked)

        self.labelSampleName = QLabel(self.trUtf8("Nazwa obrazu:"))
        self.lineEditSampleName = QLineEdit()
        self.formLayout.addRow(self.labelSampleName, self.lineEditSampleName)
        self.lineEditSampleName.textChanged.connect(self.on_sample_name_edit)

        self.buttonDelete = QtGui.QPushButton(self.trUtf8("Usuń obraz"))
        self.formLayout.addRow(self.buttonDelete)
        self.buttonDelete.clicked.connect(self.delete_image)
        # self.buttonDelete.setDisabled(True)

        self.buttonDeleteList = QtGui.QPushButton(self.trUtf8("Usuń listę obrazów"))
        self.formLayout.addRow(self.buttonDeleteList)
        self.buttonDeleteList.clicked.connect(self.delete_images_list)
        # self.buttonDeleteList.setDisabled(True)

        self.buttonClassify = QtGui.QPushButton(self.trUtf8("Klasyfikuj"))
        self.formLayout.addRow(self.buttonClassify)
        self.buttonClassify.clicked.connect(self.classify)
        # self.buttonClassify.setDisabled(True)

        self.buttonPrepareData = QtGui.QPushButton(self.trUtf8("Przygotuj dane z obrazu"))
        self.formLayout.addRow(self.buttonPrepareData)
        self.buttonPrepareData.clicked.connect(self.prepare)

        self.buttonPrepareAllData = QtGui.QPushButton(self.trUtf8("Przygotuj dane z wszystkich obrazów"))
        self.formLayout.addRow(self.buttonPrepareAllData)
        self.buttonPrepareAllData.clicked.connect(self.prepare_all)

        self.buttonPreprocess = QtGui.QPushButton(self.trUtf8("Przetwórz wstępnie"))
        self.formLayout.addRow(self.buttonPreprocess)
        self.buttonPreprocess.clicked.connect(self.preprocess)

        self.buttonPreprocessFaceDetection = QtGui.QPushButton(self.trUtf8("Przetwórz z detekcją twarzy"))
        self.formLayout.addRow(self.buttonPreprocessFaceDetection)
        self.buttonPreprocessFaceDetection.clicked.connect(self.preprocess_with_face_detection)

        self.buttonFaceDetection = QtGui.QPushButton(self.trUtf8("Wykryj twarz"))
        self.formLayout.addRow(self.buttonFaceDetection)
        self.buttonFaceDetection.clicked.connect(self.only_detect_and_crop_face)

        self.buttonLoadImage = QtGui.QPushButton(self.trUtf8("Wczytaj obraz"))
        self.formLayout.addRow(self.buttonLoadImage)
        self.buttonLoadImage.clicked.connect(self.load_image)
        # self.buttonLoadImage.setDisabled(True)

        self.buttonLoadFolder = QtGui.QPushButton(self.trUtf8("Wczytaj folder"))
        self.formLayout.addRow(self.buttonLoadFolder)
        self.buttonLoadFolder.clicked.connect(self.load_folder)
        # self.buttonLoadFolder.setDisabled(True)

        self.buttonLoadConvertFolder = QtGui.QPushButton(self.trUtf8("Wczytaj i konwertuj folder"))
        self.formLayout.addRow(self.buttonLoadConvertFolder)
        self.buttonLoadConvertFolder.clicked.connect(self.load_convert_folder)
        # self.buttonLoadFolder.setDisabled(True)

        self.buttonSaveFolder = QtGui.QPushButton(self.trUtf8("Zapisz folder"))
        self.formLayout.addRow(self.buttonSaveFolder)
        self.buttonSaveFolder.clicked.connect(self.save_folder)
        # self.buttonSaveFolder.setDisabled(True)

        self.currentImage = None

    def comboBox_selected(self, index):
        from core import Core
        Core.current_user.current_emotion = self.comboBoxEmotion.itemData(index)
        self.create_listView_from_current_emotion()

    def create_listView_from_current_emotion(self):
        self.listViewModel.clear()
        from core import Core
        for image in Core.current_user.emotions[Core.current_user.current_emotion]:
            item = QStandardItem(image.name)
            item.setData(image.name, QtCore.Qt.DisplayRole)
            item.setData(image, QtCore.Qt.UserRole)
            qimg = QImage(image.array.tostring(), image.array.shape[0], image.array.shape[1], QImage.Format_Indexed8)
            qpix = QPixmap.fromImage(qimg)
            qicon = QIcon(qpix)
            item.setIcon(qicon)
            self.listViewModel.appendRow(item)

    def add_image_to_current_emotion(self, image, name=None):
        from core import Core
        if name is None:
            name = "Obraz " + str(Core.current_user.current_image_number)
        resolution = Core.modules['opencv_webcam'].cropped_size.width()
        im = Image(image, resolution, name)
        Core.current_user.add_image_to_collection(im, Core.current_user.current_emotion)
        item = QStandardItem(im.name)
        item.setData(im.name, QtCore.Qt.DisplayRole)
        item.setData(im, QtCore.Qt.UserRole)

        qimg = QImage(im.array.tostring(), im.array.shape[0], im.array.shape[1], QImage.Format_Indexed8)
        qpix = QPixmap.fromImage(qimg)
        qicon = QIcon(qpix)
        item.setIcon(qicon)
        self.listViewModel.appendRow(item)

    def add_image_to_emotion(self, image, emotion):
        if emotion == 'neutral':
            self.comboBoxEmotion.setCurrentIndex(0)
        elif emotion == 'joy':
            self.comboBoxEmotion.setCurrentIndex(1)
        elif emotion == 'sadness':
            self.comboBoxEmotion.setCurrentIndex(2)
        elif emotion == 'anger':
            self.comboBoxEmotion.setCurrentIndex(3)
        elif emotion == 'disgust':
            self.comboBoxEmotion.setCurrentIndex(4)
        elif emotion == 'shocked':
            self.comboBoxEmotion.setCurrentIndex(5)
        self.add_image_to_current_emotion(image)

    def create_listView_from_current_user_samples(self):
        self.comboBox_selected(0)
        self.listViewModel.clear()
        self.create_listView_from_current_emotion()

    def on_listView_clicked(self, index):
        from core import Core
        Core.current_user.current_emotions_indexes[Core.current_user.current_emotion] = index.row()
        self.currentImage = index.data(QtCore.Qt.UserRole)
        self.lineEditSampleName.setText(index.data())

        image = QImage(self.currentImage.array.tostring(), self.currentImage.array.shape[1], self.currentImage.array.shape[0],
                       self.currentImage.array.strides[0], QImage.Format_Indexed8)

        if not Core.modules['opencv_webcam'].webcam_record_true:
            Core.ui['center_dock'].webcam.setPixmap(QPixmap.fromImage(image))

    def on_sample_name_edit(self, text):
        from core import Core
        if Core.current_user.current_emotions_indexes[
            Core.current_user.current_emotion] > -1 and self.currentImage is not None:
            current_emotion_index = Core.current_user.current_emotions_indexes[Core.current_user.current_emotion]
            # current_emotion = Core.current_user.emotions[Core.current_user.current_emotion]
            # current_emotion[current_emotion_index].name = text
            Core.current_user.get_current_image(Core.current_user.current_emotion).name = text
            self.currentImage.name = text
            item = self.listViewModel.item(current_emotion_index)
            item.setData(text, QtCore.Qt.DisplayRole)

    def delete_image(self):
        from core import Core
        current_emotion_index = Core.current_user.current_emotions_indexes[Core.current_user.current_emotion]
        self.listViewModel.removeRow(current_emotion_index)
        Core.current_user.delete_image(Core.current_user.current_emotion)
        self.currentImage = None

    def delete_images_list(self):
        from core import Core
        self.listViewModel.clear()
        Core.current_user.delete_images(Core.current_user.current_emotion)
        self.currentImage = None

    def load_image(self):
        from core import Core
        extension_filter = "Wszystkie pliki (*.*);;Pliki png (*.png);;Pliki jpg (*.jpg);;Pliki tiff (*.tiff)"
        path, filter = QtGui.QFileDialog.getOpenFileName(None, "Wczytaj zdjęcie", '.', extension_filter)

        if path is not None and path != "":
            head, filename = os.path.split(path)
            image = Core.modules['opencv_webcam'].read_image(path)
            if image is not None:
                # if image.shape[0] == image.shape[1]:
                image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                self.add_image_to_current_emotion(image, filename)
                # else:
                    # print "Uwaga! Nierowne wymiary obrazu!"

    def load_folder(self):
        from core import Core
        path = QtGui.QFileDialog.getExistingDirectory(None, "Wczytaj folder")
        if path is not None and path != "":
            for filename in os.listdir(path):
                if filename.endswith(".jpg") or filename.endswith(".png") or filename.endswith(".tiff") or filename.endswith(".pgm"):
                    image = Core.modules['opencv_webcam'].read_image(os.path.join(path, filename))
                    if image is not None:
                        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                        self.add_image_to_current_emotion(image, filename)
                else:
                    continue

    def save_folder(self):
        from core import Core
        path = QtGui.QFileDialog.getExistingDirectory(None, "Wczytaj folder")
        if path is not None and path != "":
            for image in Core.current_user.emotions[Core.current_user.current_emotion]:
                cv2.imwrite(os.path.join(path, image.name + ".png"), image.array)

    def classify(self):
        from core import Core
        Core.modules['neural_network_tools'].classify(Core.model, Core.current_user.emotions, Core.current_user.get_current_image(
            Core.current_user.current_emotion).array)

    def preprocess(self):
        from core import Core
        self.currentImage.array = Core.modules['preprocessing'].preprocess(self.currentImage.array)
        image = QImage(self.currentImage.array, self.currentImage.array.shape[1], self.currentImage.array.shape[0],
                       self.currentImage.array.strides[0], QImage.Format_Indexed8)
        if not Core.modules['opencv_webcam'].webcam_record_true:
            Core.ui['center_dock'].webcam.setPixmap(QPixmap.fromImage(image))

    def only_detect_and_crop_face(self):
        from core import Core
        image = self.currentImage.array
        self.currentImage.array = Core.ui['left_dock'].get_child_widget().trainingDataWindow.only_detect_and_crop_face(image)
        image = QImage(self.currentImage.array, self.currentImage.array.shape[1], self.currentImage.array.shape[0],
                       self.currentImage.array.strides[0], QImage.Format_Indexed8)
        if not Core.modules['opencv_webcam'].webcam_record_true:
            Core.ui['center_dock'].webcam.setPixmap(QPixmap.fromImage(image))

    def prepare(self):
        from core import Core
        image = self.currentImage.array
        Core.ui['left_dock'].get_child_widget().trainingDataWindow.prepare_images_from_image(image)

    def prepare_all(self):
        from core import Core
        images = []
        for image in Core.current_user.emotions[Core.current_user.current_emotion]:
            images.append(image.array)
        for image in images:
            Core.ui['left_dock'].get_child_widget().trainingDataWindow.prepare_images_from_image(image)

    def preprocess_with_face_detection(self):
        from core import Core
        x, y, w, h = Core.modules['opencv_webcam'].get_face_coordinates(self.currentImage.array, False)
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        if w > 0 and h > 0:
            image = Core.modules['opencv_webcam'].denoise(self.currentImage.array, None)
            self.currentImage.array = image[y:y + h, x:x + w]

            trainingWindow = Core.ui['left_dock'].get_child_widget().trainingDataWindow
            if trainingWindow.checkBoxPreprocessing.isChecked():
                self.currentImage.array = Core.modules['preprocessing'].preprocess(self.currentImage.array)

            if trainingWindow.checkBoxMask.isChecked():
                self.currentImage.array = Core.modules['preprocessing'].mask_image(self.currentImage.array)

            image = QImage(self.currentImage.array, self.currentImage.array.shape[1], self.currentImage.array.shape[0],
                           self.currentImage.array.strides[0], QImage.Format_Indexed8)
            if not Core.modules['opencv_webcam'].webcam_record_true:
                Core.ui['center_dock'].webcam.setPixmap(QPixmap.fromImage(image))

    def load_convert_folder(self):
        from core import Core
        path = QtGui.QFileDialog.getExistingDirectory(None, "Wczytaj folder")
        if path is not None and path != "":
            for filename in os.listdir(path):
                if filename.endswith(".jpg") or filename.endswith(".png") or filename.endswith(".tiff") or filename.endswith(".pgm"):
                    image = Core.modules['opencv_webcam'].read_image(os.path.join(path, filename))
                    if image is not None:
                        x, y, w, h = Core.modules['opencv_webcam'].get_face_coordinates(image)
                        # image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                        if w > 0 and h > 0:
                            image = Core.modules['opencv_webcam'].denoise(image, False)
                            image = image[y:y+h, x:x+w]
                            # image = cv2.resize(image, (Core.modules['opencv_webcam'].cropped_size.width(), Core.modules['opencv_webcam'].cropped_size.height()))
                            trainingWindow = Core.ui['left_dock'].get_child_widget().trainingDataWindow
                            if trainingWindow.checkBoxPreprocessing.isChecked():
                                image = Core.modules['preprocessing'].preprocess(image)
                            images = Core.modules['preprocessing'].create_rotated_images([image],trainingWindow.sint(trainingWindow.lineAmountCopyImages.text()), trainingWindow.sint(trainingWindow.lineAngle.text()))
                            images = Core.modules['preprocessing'].create_flipped_images(images, trainingWindow.checkBoxHorizontalFlip.isChecked(), trainingWindow.checkBoxVerticalFlip.isChecked())
                            if trainingWindow.checkBoxShift.isChecked():
                                images = Core.modules['preprocessing'].shift_images(images, 0.1)
                            else:
                                images = Core.modules['preprocessing'].shift_images(images, 0.0)
                            if trainingWindow.checkBoxMask.isChecked():
                                images = Core.modules['preprocessing'].mask_images(images)

                            if len(images) > 1:
                                for image in images:
                                    self.add_image_to_current_emotion(image, filename)
                            else:
                                self.add_image_to_current_emotion(image, filename)
                else:
                    continue