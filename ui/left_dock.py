# -*- coding: utf-8 -*-
import os
from functools import partial
import ntpath

import PySide
import cv2
import time
from PySide import QtGui, QtCore
from PySide.QtCore import Qt, QSize, QTimer
from PySide.QtGui import QVBoxLayout, QWidget, QFormLayout, QLabel, QLineEdit, QMessageBox, QCheckBox, QTextEdit, \
    QPixmap, QImage
import thread

from core import Core
from resources.icons import Icons
from ui.base_dock import BaseDockWidget


class LeftDockWidget(BaseDockWidget):
    def __init__(self, *args, **kwargs):
        super(LeftDockWidget, self).__init__(*args, **kwargs)
        self.setWindowTitle("Inne")

        self.dockChildWidget = LeftDockChildWidget()
        self.setWidget(self.dockChildWidget)

    def get_child_widget(self):
        return self.dockChildWidget


class LeftDockChildWidget(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.verticalLayout = QVBoxLayout(self)
        self.formLayout = QFormLayout()
        self.verticalLayout.addLayout(self.formLayout)

        self.buttonCameraSettings = QtGui.QPushButton(self.trUtf8("Ustawienia kamery"))
        self.formLayout.addRow(self.buttonCameraSettings)
        self.buttonCameraSettings.clicked.connect(self.show_camera_settings_window)

        self.buttonEnableCameraPreview = QtGui.QPushButton(self.trUtf8("Włącz podgląd kamery"))
        self.formLayout.addRow(self.buttonEnableCameraPreview)
        self.buttonEnableCameraPreview.clicked.connect(self.enableCameraPreview)

        self.buttonGetTrainingData = QtGui.QPushButton(self.trUtf8("Pobierz dane treningowe"))
        self.formLayout.addRow(self.buttonGetTrainingData)
        self.buttonGetTrainingData.clicked.connect(self.show_training_data_window)

        self.buttonTrainNetwork = QtGui.QPushButton(self.trUtf8("Ucz sieć neuronową"))
        self.formLayout.addRow(self.buttonTrainNetwork)
        self.buttonTrainNetwork.clicked.connect(self.train_network)

        self.buttonTestNetwork = QtGui.QPushButton(self.trUtf8("Testuj sieć"))
        self.formLayout.addRow(self.buttonTestNetwork)
        self.buttonTestNetwork.clicked.connect(self.show_test_window)

        self.buttonSetCNN = QtGui.QPushButton(self.trUtf8("Ustaw sieć CNN"))
        self.formLayout.addRow(self.buttonSetCNN)
        self.buttonSetCNN.clicked.connect(self.set_cnn)

        # self.buttonSetRNN = QtGui.QPushButton(self.trUtf8("Ustaw sieć RNN"))
        # self.formLayout.addRow(self.buttonSetRNN)
        # self.buttonSetRNN.clicked.connect(self.set_rnn)

        self.buttonSetMLP = QtGui.QPushButton(self.trUtf8("Ustaw sieć MLP"))
        self.formLayout.addRow(self.buttonSetMLP)
        self.buttonSetMLP.clicked.connect(self.set_mlp)

        # self.buttonTakeImage = QtGui.QPushButton(self.trUtf8("Zrób zdjęcie"))
        # self.formLayout.addRow(self.buttonTakeImage)
        # self.buttonTakeImage.clicked.connect(self.take_image)

        self.trainingDataWindow = TrainingDataWindow()
        self.cameraSettingsWindow = CameraSettingsWindow()
        self.testWindow = TestWindow()

    def set_cnn(self):
        from neural_network.neural_network_tools_cnn import NeuralNetModuleCNN
        Core.modules['neural_network_tools'] = NeuralNetModuleCNN()

    def set_rnn(self):
        from neural_network.neural_network_tools_rnn import NeuralNetModuleRNN
        Core.modules['neural_network_tools'] = NeuralNetModuleRNN()

    def set_mlp(self):
        from neural_network.neural_network_tools_mlp import NeuralNetModuleMLP
        Core.modules['neural_network_tools'] = NeuralNetModuleMLP()

    def enableCameraPreview(self):
        if Core.modules['opencv_webcam'].webcam_record_true:
            Core.modules['opencv_webcam'].webcam_record_true = False
        else:
            Core.modules['opencv_webcam'].webcam_record_true = True

    def show_training_data_window(self):
        self.trainingDataWindow.show()
        self.trainingDataWindow.activateWindow()
        self.trainingDataWindow.raise_()

    def show_camera_settings_window(self):
        self.cameraSettingsWindow.prepare_window_to_show()
        self.cameraSettingsWindow.show()
        self.cameraSettingsWindow.activateWindow()
        self.cameraSettingsWindow.raise_()

    def show_test_window(self):
        self.testWindow.prepare_window_to_show()
        self.testWindow.show()
        self.testWindow.activateWindow()
        self.testWindow.raise_()

    def train_network(self):
        neural_net_module = Core.modules['neural_network_tools']
        #start = time.time()
        Core.model = neural_net_module.create_model(Core.users)
        #end = time.time()
        #print "Time of learning network: " + str(end - start) + " s"
        #print "Time of learning epoch: " + str((end - start)/Core.modules['neural_network_tools'].nb_epoch) + " s"

    def take_image(self):
        image = Core.modules['opencv_webcam'].take_photo()
        if image is not None:
            image = Core.modules['preprocessing'].preprocess(image)
            cv2.imshow("Image", image)


class TestWindow(QWidget):
    def __init__(self, *args, **kwargs):
        super(TestWindow, self).__init__(*args, **kwargs)
        self.resize(250, 250)
        self.setWindowTitle("Testowanie")
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)

        self.verticalLayout = QVBoxLayout(self)
        self.formLayout = QFormLayout()
        self.verticalLayout.addLayout(self.formLayout)

        self.webcam_image = QLabel()
        self.formLayout.addRow(self.webcam_image)

        self.textEditEmotionLog = QTextEdit()
        self.formLayout.addRow(self.textEditEmotionLog)

        self.buttonClear = QtGui.QPushButton(self.trUtf8("Wyczyść"))
        self.buttonClear.clicked.connect(self.clear_text)
        self.formLayout.addRow(self.buttonClear)

        self.buttonStop = QtGui.QPushButton(self.trUtf8("STOP"))
        self.buttonStop.clicked.connect(self.stop)
        self.formLayout.addRow(self.buttonStop)

        self.timer = QTimer()
        self.timer.timeout.connect(self.test)
        self.timer.setInterval(1000)

    def closeEvent(self, event):
        self.stop()
        event.accept()

    def prepare_window_to_show(self):
        self.timer.start(1000)

    def clear_text(self):
        self.textEditEmotionLog.setText("")

    def test(self):
        image = Core.modules['opencv_webcam'].take_photo()
        if image is not None:
            image = Core.modules['preprocessing'].preprocess(image)
            if Core.ui['left_dock'].get_child_widget().trainingDataWindow.checkBoxMask.isChecked():
                image = Core.modules['preprocessing'].mask_image(image)
            image_obj = QImage(image, image.shape[1], image.shape[0],
                       image.strides[0], QImage.Format_Indexed8)
            self.webcam_image.setPixmap(QPixmap.fromImage(image_obj))

            (emotion, result) = Core.modules['neural_network_tools'].classify(Core.model, Core.current_user.emotions, image)
            line = '\n' + str(emotion) + ' --> ' + str(result)
            self.textEditEmotionLog.setText(line + self.textEditEmotionLog.toPlainText())

    def stop(self):
        self.timer.stop()
        self.hide()


class CameraSettingsWindow(QWidget):
    def __init__(self, *args, **kwargs):
        super(CameraSettingsWindow, self).__init__(*args, **kwargs)
        self.resize(250, 150)
        self.setWindowTitle("Ustawienia kamery")
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)

        self.verticalLayout = QVBoxLayout(self)
        self.formLayout = QFormLayout()
        self.verticalLayout.addLayout(self.formLayout)

        self.labelCameraNumber = QLabel(self.trUtf8("Numer kamery:"))
        self.lineCameraNumber = QLineEdit()
        self.formLayout.addRow(self.labelCameraNumber, self.lineCameraNumber)

        self.labelFlipY = QLabel(self.trUtf8("Obróć w osi Y:"))
        self.checkBoxFlipY = QCheckBox()
        self.formLayout.addRow(self.labelFlipY, self.checkBoxFlipY)

        self.labelFlipX = QLabel(self.trUtf8("Obróć w osi X:"))
        self.checkBoxFlipX = QCheckBox()
        self.formLayout.addRow(self.labelFlipX, self.checkBoxFlipX)

        self.labelBGRtoRGB = QLabel(self.trUtf8("BGR -> RGB:"))
        self.checkBoxBGRtoRGB = QCheckBox()
        self.formLayout.addRow(self.labelBGRtoRGB, self.checkBoxBGRtoRGB)

        self.buttonOK = QtGui.QPushButton(self.trUtf8("OK"))
        self.buttonOK.clicked.connect(self.accept_changes)

        self.buttonCancel = QtGui.QPushButton(self.trUtf8("Anuluj"))
        self.formLayout.addRow(self.buttonCancel, self.buttonOK)
        self.buttonCancel.clicked.connect(self.discard_changes)

        self.sfloat = self.ignore_exception(ValueError)(float)
        self.sint = self.ignore_exception(ValueError)(int)

    def prepare_window_to_show(self):
        self.lineCameraNumber.setText(str(Core.modules['opencv_webcam'].camera_number))
        self.checkBoxFlipY.setChecked(Core.modules['opencv_webcam'].flipY)
        self.checkBoxFlipX.setChecked(Core.modules['opencv_webcam'].flipX)
        self.checkBoxBGRtoRGB.setChecked(Core.modules['opencv_webcam'].colorBGRtoRGB)
        # widget.text = webcamcv wartosc
        pass

    def accept_changes(self):
        Core.modules['opencv_webcam'].camera_number = self.sint(self.lineCameraNumber.text())
        Core.modules['opencv_webcam'].flipY = self.checkBoxFlipY.isChecked()
        Core.modules['opencv_webcam'].flipX = self.checkBoxFlipX.isChecked()
        Core.modules['opencv_webcam'].colorBGRtoRGB = self.checkBoxBGRtoRGB.isChecked()

        Core.modules['opencv_webcam'].release_camera()
        Core.modules['opencv_webcam'].initialize_module()
        self.hide()

    def discard_changes(self):
        self.hide()

    def ignore_exception(ignore_exception=Exception, default_value=0):
        def dec(function):
            def _dec(*args, **kwargs):
                try:
                    return function(*args, **kwargs)
                except ignore_exception:
                    return default_value
            return _dec
        return dec


class TrainingDataWindow(QWidget):
    def __init__(self, *args, **kwargs):
        super(TrainingDataWindow, self).__init__(*args, **kwargs)
        self.resize(300, 150)
        self.setWindowTitle("Pobieranie danych treningowych")
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)

        self.verticalLayout = QVBoxLayout(self)
        self.formLayout = QFormLayout()
        self.verticalLayout.addLayout(self.formLayout)

        self.buttonDefault = QtGui.QPushButton(self.trUtf8("Domyślne"))
        self.formLayout.addRow(self.buttonDefault)
        self.buttonDefault.clicked.connect(self.reset_to_default_settings)

        self.labelPreprocessing = QLabel(self.trUtf8("Użyj preprocessingu:"))
        self.checkBoxPreprocessing = QCheckBox()
        self.checkBoxPreprocessing.setChecked(True)
        self.formLayout.addRow(self.labelPreprocessing, self.checkBoxPreprocessing)

        self.labelAmountImages = QLabel(self.trUtf8("Ilość zdjęć:"))
        self.lineAmountImages = QLineEdit("3")
        self.formLayout.addRow(self.labelAmountImages, self.lineAmountImages)

        self.labelAmountCopyImages = QLabel(self.trUtf8("Ilość obróconych kopii na każde zdjęcie:"))
        self.lineAmountCopyImages = QLineEdit("3")
        self.formLayout.addRow(self.labelAmountCopyImages, self.lineAmountCopyImages)

        self.labelAngle = QLabel(self.trUtf8("Kąt obrotu:"))
        self.lineAngle = QLineEdit("3")
        self.formLayout.addRow(self.labelAngle, self.lineAngle)

        self.labelHorizontalFlip = QLabel(self.trUtf8("Twórz kopie obrócone w poziomie:"))
        self.checkBoxHorizontalFlip = QCheckBox()
        self.checkBoxHorizontalFlip.setChecked(False)
        self.formLayout.addRow(self.labelHorizontalFlip, self.checkBoxHorizontalFlip)

        self.labelVerticalFlip = QLabel(self.trUtf8("Twórz kopie obrócone w pionie:"))
        self.checkBoxVerticalFlip = QCheckBox()
        self.checkBoxVerticalFlip.setChecked(False)
        # self.formLayout.addRow(self.labelVerticalFlip, self.checkBoxVerticalFlip)

        self.labelMask = QLabel(self.trUtf8("Maskuj obrazy:"))
        self.checkBoxMask = QCheckBox()
        self.checkBoxMask.setChecked(False)
        self.formLayout.addRow(self.labelMask, self.checkBoxMask)

        self.labelShift = QLabel(self.trUtf8("Przesuwaj obrazy:"))
        self.checkBoxShift = QCheckBox()
        self.checkBoxShift.setChecked(True)
        self.formLayout.addRow(self.labelShift, self.checkBoxShift)

        self.buttonNeutral = QtGui.QPushButton(self.trUtf8("Neutralność"))
        self.buttonNeutral.setIcon(Core.modules['icons'].iconNeutral)
        self.buttonNeutral.setIconSize(QSize(40, 40))
        self.formLayout.addRow(self.buttonNeutral)
        self.buttonNeutral.clicked.connect(partial(self.take_images, 'neutral'))

        self.buttonJoy = QtGui.QPushButton(self.trUtf8("Radość"))
        self.buttonJoy.setIcon(Core.modules['icons'].iconJoy)
        self.buttonJoy.setIconSize(QSize(40, 40))
        self.formLayout.addRow(self.buttonJoy)
        self.buttonJoy.clicked.connect(partial(self.take_images, 'joy'))

        self.buttonSadness = QtGui.QPushButton(self.trUtf8("Smutek"))
        self.buttonSadness.setIcon(Core.modules['icons'].iconSadness)
        self.buttonSadness.setIconSize(QSize(40, 40))
        self.formLayout.addRow(self.buttonSadness)
        self.buttonSadness.clicked.connect(partial(self.take_images, 'sadness'))

        self.buttonAnger = QtGui.QPushButton(self.trUtf8("Wściekłość"))
        self.buttonAnger.setIcon(Core.modules['icons'].iconAnger)
        self.buttonAnger.setIconSize(QSize(40, 40))
        self.formLayout.addRow(self.buttonAnger)
        self.buttonAnger.clicked.connect(partial(self.take_images, 'anger'))

        self.buttonDisgust = QtGui.QPushButton(self.trUtf8("Obrzydzenie"))
        self.buttonDisgust.setIcon(Core.modules['icons'].iconDisgust)
        self.buttonDisgust.setIconSize(QSize(40, 40))
        self.formLayout.addRow(self.buttonDisgust)
        self.buttonDisgust.clicked.connect(partial(self.take_images, 'disgust'))

        self.buttonShocked = QtGui.QPushButton(self.trUtf8("Zaskoczenie"))
        self.buttonShocked.setIcon(Core.modules['icons'].iconShocked)
        self.buttonShocked.setIconSize(QSize(40, 40))
        self.formLayout.addRow(self.buttonShocked)
        self.buttonShocked.clicked.connect(partial(self.take_images, 'shocked'))

        self.buttonTakeImage = QtGui.QPushButton(self.trUtf8("Zrób zdjęcie dla bieżącej emocji"))
        self.formLayout.addRow(self.buttonTakeImage)
        self.buttonTakeImage.clicked.connect(self.take_image)

        self.buttonCancel = QtGui.QPushButton(self.trUtf8("Anuluj"))
        self.formLayout.addRow(self.buttonCancel)
        self.buttonCancel.clicked.connect(self.discard_changes)

        self.sfloat = self.ignore_exception(ValueError)(float)
        self.sint = self.ignore_exception(ValueError)(int)

    def reset_to_default_settings(self):
        self.checkBoxPreprocessing.setChecked(True)
        self.lineAmountImages.setText("3")
        self.lineAmountCopyImages.setText("3")
        self.lineAngle.setText("3")
        self.checkBoxHorizontalFlip.setChecked(False)
        self.checkBoxMask.setChecked(False)
        self.checkBoxShift.setChecked(True)

    def take_image(self):
        photo = None
        while photo is None:
            photo = Core.modules['opencv_webcam'].take_photo()
        if self.checkBoxPreprocessing.isChecked():
            photo = Core.modules['preprocessing'].preprocess(photo)
        if self.checkBoxMask.isChecked():
            photo = Core.modules['preprocessing'].mask_image(photo)
        Core.ui['right_dock'].get_child_widget().add_image_to_emotion(photo, Core.current_user.current_emotion)

    def take_images(self, emotion):
        photos = Core.modules['opencv_webcam'].take_photos(self.sint(self.lineAmountImages.text()))
        if self.checkBoxPreprocessing.isChecked():
            photos = Core.modules['preprocessing'].preprocess_multiple(photos)

        if self.checkBoxHorizontalFlip.isChecked():
            photos = Core.modules['preprocessing'].create_flipped_images(photos, True, False)

        photos = Core.modules['preprocessing'].create_rotated_images(photos, self.sint(self.lineAmountCopyImages.text()), self.sint(self.lineAngle.text()))
        if self.checkBoxShift.isChecked():
            photos = Core.modules['preprocessing'].shift_images(photos, 0.1)
        else:
            photos = Core.modules['preprocessing'].shift_images(photos, 0.0)

        if self.checkBoxMask.isChecked():
            photos = Core.modules['preprocessing'].mask_images(photos)

        for photo in photos:
            Core.ui['right_dock'].get_child_widget().add_image_to_emotion(photo, emotion)

    def only_detect_and_crop_face(self, image):
        x, y, w, h = Core.modules['opencv_webcam'].get_face_coordinates(image, False)
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        if w > 0 and h > 0:
            image = image[y:y + h, x:x + w]
            image = Core.modules['opencv_webcam'].denoise(image, None)
            image = cv2.resize(image, (
            Core.modules['opencv_webcam'].cropped_size.width(), Core.modules['opencv_webcam'].cropped_size.height()))
            return image

    def prepare_images_from_image(self, image):
        images = []
        images.append(image)
        photos = []

        for image in images:
            x, y, w, h = Core.modules['opencv_webcam'].get_face_coordinates(image, False)
            # image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            if w > 0 and h > 0:
                image = image[y:y + h, x:x + w]
                image = Core.modules['opencv_webcam'].denoise(image, None)
                image = cv2.resize(image, (Core.modules['opencv_webcam'].cropped_size.width(), Core.modules['opencv_webcam'].cropped_size.height()))
                photos.append(image)

        if self.checkBoxPreprocessing.isChecked():
            photos = Core.modules['preprocessing'].preprocess_multiple(photos)

        if self.checkBoxHorizontalFlip.isChecked():
            photos = Core.modules['preprocessing'].create_flipped_images(photos, True, False)

        photos = Core.modules['preprocessing'].create_rotated_images(photos,
                                                                     self.sint(self.lineAmountCopyImages.text()),
                                                                     self.sint(self.lineAngle.text()))
        if self.checkBoxShift.isChecked():
            photos = Core.modules['preprocessing'].shift_images(photos, 0.1)
        else:
            photos = Core.modules['preprocessing'].shift_images(photos, 0.0)

        if self.checkBoxMask.isChecked():
            photos = Core.modules['preprocessing'].mask_images(photos)

        for photo in photos:
            Core.ui['right_dock'].get_child_widget().add_image_to_current_emotion(photo)

    def discard_changes(self):
        self.hide()

    def ignore_exception(ignore_exception=Exception, default_value=0.0):
        def dec(function):
            def _dec(*args, **kwargs):
                try:
                    return function(*args, **kwargs)
                except ignore_exception:
                    return default_value
            return _dec
        return dec

