from PySide.QtCore import Qt, QSize
from PySide.QtGui import QWidget, QVBoxLayout, QScrollArea, QLabel, QPushButton


class CenterDockWidget(QWidget):
    def __init__(self, *args, **kwargs):
        super(CenterDockWidget, self).__init__(*args, **kwargs)

        self.mainVerticalLayout = QVBoxLayout()
        self.centerWidget = QWidget()
        self.centerWidget.setLayout(self.mainVerticalLayout)

        self.scrollArea = QScrollArea()
        self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.centerWidget)

        self.verticalLayout = QVBoxLayout(self)
        self.verticalLayout.addWidget(self.scrollArea)
        self.setLayout(self.verticalLayout)

        self.webcam = QLabel()
        self.mainVerticalLayout.addWidget(self.webcam)

        self.zoomPlusButton = QPushButton("Zoom + ")
        self.mainVerticalLayout.addWidget(self.zoomPlusButton)
        self.zoomPlusButton.clicked.connect(self.zoom_plus_image)

        self.zoomMinusButton = QPushButton("Zoom - ")
        self.mainVerticalLayout.addWidget(self.zoomMinusButton)
        self.zoomMinusButton.clicked.connect(self.zoom_minus_image)

    def zoom_plus_image(self):
        p = self.webcam.pixmap()
        w = p.width()
        h = p.height()
        self.webcam.setPixmap(p.scaled(w*2,h*2, Qt.KeepAspectRatio))

    def zoom_minus_image(self):
        p = self.webcam.pixmap()
        w = p.width()
        h = p.height()
        self.webcam.setPixmap(p.scaled(w/2,h/2, Qt.KeepAspectRatio))