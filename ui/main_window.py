# -*- coding: utf-8 -*-
from PySide import QtGui, QtCore
from PySide.QtCore import Qt
from core import Core
from resources.css import StylesheetClass
from ui.center_panel import CenterDockWidget
from ui.left_dock import LeftDockWidget
from ui.right_dock import RightDockWidget
from ui.user_dock import UserDockWidget


class MainWindow(QtGui.QMainWindow, StylesheetClass):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setWindowTitle(self.trUtf8("CamEMO 0.1 - rozpoznawanie emocji na podstawie obrazu"))
        self.resize(1024, 768)
        self.setDockNestingEnabled(True)
        self.applyStylesheet()

        self.left_dock = LeftDockWidget()
        Core.ui['left_dock'] = self.left_dock

        self.right_dock = RightDockWidget()
        Core.ui['right_dock'] = self.right_dock

        self.center_dock = CenterDockWidget()
        Core.ui['center_dock'] = self.center_dock

        self.user_dock = UserDockWidget()
        Core.ui['user_dock'] = self.user_dock

        self.addDockWidget(Qt.LeftDockWidgetArea, self.left_dock)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.user_dock)
        self.addDockWidget(Qt.RightDockWidgetArea, self.right_dock)
        self.setCentralWidget(self.center_dock)

