# -*- coding: utf-8 -*-
from image.openCV_webcam import OpenCVWebCamModule
from image.preprocessing import PreprocessingModule
from resources.icons import Icons
from users.user import User
import pickle

from neural_network.neural_network_tools_cnn import NeuralNetModuleCNN


class Core:
    modules = {}
    ui = {}
    users = []
    current_user = None
    current_user_index = 0
    model = None

    @classmethod
    def initialize_modules(cls):
        user = User()
        user.name = unicode("Domyślny", "utf-8")
        cls.users.append(user)
        cls.current_user = user

        cls.modules['icons'] = Icons()
        cls.modules['opencv_webcam'] = OpenCVWebCamModule()
        cls.modules['neural_network_tools'] = NeuralNetModuleCNN()
        cls.modules['preprocessing'] = PreprocessingModule()

        for module in cls.modules:
            cls.modules[module].initialize_module()

    @classmethod
    def import_user_base_from_file(cls, path):
        cls.users = pickle.load(open(path, "rb"))
        cls.current_user = cls.users[0]
        cls.ui['user_dock'].get_child_widget().create_users_from_Core()
        cls.ui['right_dock'].get_child_widget().create_listView_from_current_user_samples()

    @classmethod
    def export_user_base_from_file(cls, path):
        pickle.dump(cls.users, open(path, "wb"))