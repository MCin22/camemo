
class User:
    def __init__(self):
        self.name = "Nowy"
        self.emotions = {
            'neutral': [],
            'joy': [],
            'sadness': [],
            'anger': [],
            'disgust': [],
            'shocked': []
        }
        self.current_emotions_indexes = {
            'neutral': -1,
            'joy': -1,
            'sadness': -1,
            'anger': -1,
            'disgust': -1,
            'shocked': -1
        }

        self.current_emotion = 'neutral'
        self.current_image_number = 0

    def add_image_to_collection(self, image, emotion):
        self.emotions[emotion].append(image)
        self.current_emotions_indexes[emotion] = len(self.emotions[emotion]) - 1
        self.current_image_number += 1

    def delete_image(self, emotion):
        if self.current_emotions_indexes[emotion] > -1:
            self.emotions[emotion].remove(self.emotions[emotion][self.current_emotions_indexes[emotion]])
            self.current_emotions_indexes[emotion] = -1

    def delete_images(self, emotion):
        self.emotions[emotion] = []
        self.current_emotions_indexes[emotion] = -1

    def get_current_image(self, emotion):
        return self.emotions[emotion][self.current_emotions_indexes[emotion]]
