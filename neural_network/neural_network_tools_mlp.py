# import os
# orig_flags = os.environ.get('THEANO_FLAGS', '')
# os.environ['THEANO_FLAGS'] = orig_flags + "mode=FAST_RUN,device=gpu,floatX=float32"
import time

__author__ = 'MCin22'

import numpy as np
np.random.seed(1337)

from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.optimizers import SGD, Adam, RMSprop
from keras.utils import np_utils

class NeuralNetModuleMLP:
    def __init__(self):
        self.initialize_module()

    def initialize_module(self):
        from core import Core
        self.img_rows = int(Core.modules['opencv_webcam'].cropped_size.width())
        self.img_cols = int(Core.modules['opencv_webcam'].cropped_size.height())
        self.img_channels = 1
        self.nb_classes = 0
        for emotion, list in Core.current_user.emotions.iteritems():
            self.nb_classes += 1
        self.batch_size = 32
        self.nb_epoch = 300
        self.data_augmentation = False
        self.test_amount_per_emotion = 1

    def create_model(self, users):
        return self.train()

    def prepare_data(self):
        nb_train_samples = 0
        nb_test_samples = 0

        # policzenie ilosci zdjec w bazie
        from core import Core
        for emotion, list in Core.current_user.emotions.iteritems():
            nb_train_samples += len(list)
            if len(list) >= self.test_amount_per_emotion:
                nb_test_samples += self.test_amount_per_emotion
                nb_train_samples -= self.test_amount_per_emotion

        # zbudowanie identyfikatorow dla kazdej emocji (neutral = 0, joy = 1, etc)
        emotion_dict = {}
        i = 0
        for emotion, list in Core.current_user.emotions.iteritems():
            emotion_dict[emotion] = i
            i += 1

        # zbudowanie pustych macierzy treningowych i testowych
        X_train = np.zeros((nb_train_samples, self.img_rows * self.img_cols), dtype="uint8")
        y_train = np.zeros((nb_train_samples,), dtype="uint8")
        X_test = np.zeros((nb_test_samples, self.img_rows * self.img_cols), dtype="uint8")
        y_test = np.zeros((nb_test_samples,), dtype="uint8")

        # uzupelnienie macierzy testowych i treningowych o dane
        i = 0
        for emotion, list in Core.current_user.emotions.iteritems():
            for item in list:
                if i < self.test_amount_per_emotion:
                    X_test[i, :] = np.array(item.array).flatten()
                    y_test[i] = emotion_dict[emotion]
                else:
                    X_train[i - nb_test_samples, :] = np.array(item.array).flatten()
                    y_train[i - nb_test_samples] = emotion_dict[emotion]
                i += 1

        y_train = np.reshape(y_train, (len(y_train)))
        y_test = np.reshape(y_test, (len(y_test)))

        return (X_train, y_train), (X_test, y_test)

    def build_network(self):
        np.random.seed(1337)
        model = Sequential()

        # -----------------------------------------

        model.add(Dense(512, input_shape=(self.img_rows * self.img_cols,)))
        model.add(Activation('relu'))
        model.add(Dropout(0.2))
        model.add(Dense(512))
        model.add(Activation('relu'))
        model.add(Dropout(0.2))

        model.add(Dense(1024))
        model.add(Activation('relu'))
        model.add(Dropout(0.2))
        model.add(Dense(1024))
        model.add(Activation('relu'))
        model.add(Dropout(0.2))

        model.add(Dense(self.nb_classes))
        model.add(Activation('softmax'))

        # -----------------------------------------

        # model.add(Dense(512, input_shape=(self.img_rows * self.img_cols,)))
        # model.add(Activation('relu'))
        # model.add(Dropout(0.2))
        # model.add(Dense(512))
        # model.add(Activation('relu'))
        # model.add(Dropout(0.2))
        #
        # model.add(Dense(self.nb_classes))
        # model.add(Activation('softmax'))

        # -----------------------------------------

        # model.summary()

        model.compile(loss='categorical_crossentropy', optimizer=RMSprop(), metrics=["accuracy"])

        return model

    def train(self):
        '''
        :param emotion_list: Lista par (nazwa_uzytkownika, cechy)
        :return: Model sieci
        '''

        (X_train, y_train), (X_test, y_test) = self.prepare_data()

        print('X_train shape:', X_train.shape)
        print(X_train.shape[0], 'train samples')
        print(X_test.shape[0], 'test samples')

        Y_train = np_utils.to_categorical(y_train, self.nb_classes)
        Y_test = np_utils.to_categorical(y_test, self.nb_classes)

        model = self.build_network()

        X_train = X_train.astype('float32')
        X_test = X_test.astype('float32')
        X_train /= 255
        X_test /= 255

        start = time.time()
        model.fit(X_train, Y_train, batch_size=self.batch_size,
                  nb_epoch=self.nb_epoch,
                  validation_data=(X_test, Y_test), shuffle=True)
        end = time.time()
        print "Time of learning network: " + str(end - start) + " s"
        print "Time of learning epoch: " + str((end - start) / self.nb_epoch) + " s"

        if len(X_test) > 0:
            score = model.evaluate(X_test, Y_test)
            print('Test score:', score[0])
            print('Test accuracy:', score[1])

        model.save_weights('network.model', overwrite=True)

        return model

    def classify(self, model, emotions, image):
        if model is None:
            from core import Core
            model = Core.model
        if model is None:
            pass

        X_test = np.zeros((1, self.img_rows * self.img_cols), dtype="uint8")
        X_test[0, :] = np.array(image).flatten()

        X_test = X_test.astype('float32')
        X_test /= 255

        if model is None:
            model = self.build_network()
            model.load_weights('network.model')

        classified = model.predict_proba(X_test)
        argmax = classified[0].argmax()

        best_emotion = ''
        best_value = 0
        print('--------------------------')
        for (i, emotion) in enumerate(emotions):
            classify = classified[0, i]
            arrow = '-->'

            if i == argmax:
                arrow = '>>>'
                best_emotion = emotion
                best_value = round(classify, 4)

            print(str(emotion) + '\t ' + str(arrow) + '  ' + str(round(classify, 4)))

        return (best_emotion, best_value)
