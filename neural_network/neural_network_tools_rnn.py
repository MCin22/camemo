# import os
# orig_flags = os.environ.get('THEANO_FLAGS', '')
# os.environ['THEANO_FLAGS'] = orig_flags + "mode=FAST_RUN,device=gpu,floatX=float32"
from keras.initializations import normal, identity
from keras.layers import SimpleRNN
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.layers.normalization import BatchNormalization
from keras.preprocessing.image import ImageDataGenerator

__author__ = 'MCin22'

import numpy as np
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.optimizers import SGD, Adam, RMSprop
from keras.utils import np_utils

class NeuralNetModuleRNN:
    def __init__(self):
        self.initialize_module()

    def initialize_module(self):
        from core import Core
        self.img_rows = int(Core.modules['opencv_webcam'].cropped_size.width())
        self.img_cols = int(Core.modules['opencv_webcam'].cropped_size.height())
        self.img_channels = 1
        self.nb_classes = 0
        for emotion, list in Core.current_user.emotions.iteritems():
            self.nb_classes += 1
        self.batch_size = 32
        self.nb_epoch = 300
        self.test_amount_per_emotion = 1

        self.hidden_units = 100

    def create_model(self, users):
        return self.train()

    def prepare_data(self):
        nb_train_samples = 0
        nb_test_samples = 0

        # policzenie ilosci zdjec w bazie
        from core import Core
        for emotion, list in Core.current_user.emotions.iteritems():
            nb_train_samples += len(list)
            if len(list) >= self.test_amount_per_emotion:
                nb_test_samples += self.test_amount_per_emotion
                nb_train_samples -= self.test_amount_per_emotion

        # zbudowanie identyfikatorow dla kazdej emocji (neutral = 0, joy = 1, etc)
        emotion_dict = {}
        i = 0
        for emotion, list in Core.current_user.emotions.iteritems():
            emotion_dict[emotion] = i
            i += 1

        # zbudowanie pustych macierzy treningowych i testowych
        X_train = np.zeros((nb_train_samples, self.img_rows * self.img_cols, 1), dtype="uint8")
        y_train = np.zeros((nb_train_samples,), dtype="uint8")
        X_test = np.zeros((nb_test_samples, self.img_rows * self.img_cols, 1), dtype="uint8")
        y_test = np.zeros((nb_test_samples,), dtype="uint8")

        # uzupelnienie macierzy testowych i treningowych o dane
        i = 0
        for emotion, list in Core.current_user.emotions.iteritems():
            for item in list:
                if i < self.test_amount_per_emotion:
                    X_test[i, :, 0] = np.array(item.array).flatten()
                    y_test[i] = emotion_dict[emotion]
                else:
                    X_train[i - nb_test_samples, :, 0] = np.array(item.array).flatten()
                    y_train[i - nb_test_samples] = emotion_dict[emotion]
                i += 1

        y_train = np.reshape(y_train, (len(y_train), 1))
        y_test = np.reshape(y_test, (len(y_test), 1))

        return (X_train, y_train), (X_test, y_test)

    def build_network(self):
        model = Sequential()
        model.add(SimpleRNN(output_dim=self.hidden_units,
                            init=lambda shape, name: normal(shape, scale=0.001, name=name),
                            inner_init=lambda shape, name: identity(shape, scale=1.0, name=name),
                            activation='relu',
                            input_shape=[self.img_cols, self.img_rows]))

        model.add(Dense(self.nb_classes))
        model.add(Activation('softmax'))

        rmsprop = RMSprop(lr=1e-6)
        model.compile(loss='categorical_crossentropy', optimizer=rmsprop)

        return model

    def train(self):
        '''
        :param emotion_list: Lista par (nazwa_uzytkownika, cechy)
        :return: Model sieci
        '''

        (X_train, y_train), (X_test, y_test) = self.prepare_data()

        X_train = X_train.reshape(X_train.shape[0], -1, 1)
        X_test = X_test.reshape(X_test.shape[0], -1, 1)
        X_train = X_train.astype('float32')
        X_test = X_test.astype('float32')
        X_train /= 255
        X_test /= 255

        print('X_train shape:', X_train.shape)
        print(X_train.shape[0], 'train samples')
        print(X_test.shape[0], 'test samples')

        Y_train = np_utils.to_categorical(y_train, self.nb_classes)
        Y_test = np_utils.to_categorical(y_test, self.nb_classes)

        model = self.build_network()

        model.fit(X_train, Y_train, batch_size=self.batch_size,
                  nb_epoch=self.nb_epoch, show_accuracy=True,
                  validation_data=(X_test, Y_test), shuffle=True)

        if len(X_test) > 0:
            score = model.evaluate(X_test, Y_test, show_accuracy=True)
            print('Test score:', score[0])
            print('Test accuracy:', score[1])

        model.save_weights('network.model', overwrite=True)

        return model

    def classify(self, model, emotions, image):
        if model is None:
            from core import Core
            model = Core.model
        if model is None:
            pass

        X_test = np.zeros((1, self.img_rows * self.img_cols, 1), dtype="uint8")
        X_test[0, self.img_rows * self.img_cols, 0] = np.array(image)

        X_test = X_test.astype('float32')
        X_test /= 255

        if model is None:
            model = self.build_network()
            model.load_weights('network.model')

        classified = model.predict_proba(X_test)
        argmax = classified[0].argmax()

        best_emotion = ''
        best_value = 0
        print('--------------------------')
        for (i, emotion) in enumerate(emotions):
            classify = classified[0, i]
            arrow = '-->'

            if i == argmax:
                arrow = '>>>'
                best_emotion = emotion
                best_value = round(classify, 4)

            print(str(emotion) + '\t ' + str(arrow) + '  ' + str(round(classify, 4)))

        return (best_emotion, best_value)
