import sys

from PySide.QtGui import QApplication
from core import Core
from ui.main_window import MainWindow


class CoreApp:
    def __init__(self):
        Core.initialize_modules()
        self.main_window = MainWindow()
        self.main_window.show()

def main():
    app = QApplication(sys.argv)
    core_app = CoreApp()
    core_app.main_window.show()
    app.exec_()
    sys.exit()

if __name__ == '__main__':
    main()